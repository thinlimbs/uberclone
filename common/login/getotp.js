//import liraries
import React, {useState} from 'react';
import {View, Text, TextInput,Button,TouchableOpacity} from 'react-native';
import {styles, InputBox, Textfeild,ButtonFeild} from '../../common';
// import { useState } from 'react';

// create a component
const GetOtp = ({navigation}) => {
  const [enterOtp, setEnterOtp] = useState(0);
  return (
    <View style={styles.containerCenter}>
      <Textfeild  fontWeight={'500'}
      fontSize={16}
      paddingTop={10}
      content={'Enter your otp'} />
    
        <InputBox
          marginTop={20}
          width={200}
          
          value={enterOtp}
          letterSpacing={10}
          fontSize={24}
          maxLength={6}
          onChangeText={text => setEnterOtp(text)}
        />
        
       
           <ButtonFeild
     onPress={() => navigation.navigate("homebooking")}
           fontWeight={'500'}
           fontSize={12}
           letterSpacing={3}
      
      paddingTop={10} 
      width={100} 
      content="SUBMIT"
      />

       
     
 
    </View>
  );
};

// define your styles
// const styles = StyleSheet.create({
//     container: {
//         flex: 1,
//         justifyContent: 'center',
//         alignItems: 'center',
//         // backgroundColor: '#2c3e50',
//     },
// });

//make this component available to the app
export default GetOtp;
