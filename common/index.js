import React, {Component} from 'react';
import {
  View,
  SafeAreaView,
  Button,
  Text,
  StyleSheet,
  TouchableOpacity,
  TextInput,
} from 'react-native';
function padding(a, b, c, d) {
  return {
    paddingTop: a,
    paddingBottom: b ? b : a,
    paddingRight: c ? c : a,
    paddingLeft: d ? d : b ? b : a,
  };
}

function margin(a, b, c, d) {
  return {
    marginTop: a,
    marginBottom: b ? b : a,
    marginLeft: c ? c : a,
    marginRight: d ? d : b ? b : a,
  };
}
export const MyComponent = props => {
  return (
    <View style={styles.container}>
      <Text>MyComponent</Text>
    </View>
  );
};

export const MyComponent2 = ({navigation}) => {
  return (
    <SafeAreaView style={styles.container}>
      <Button
        title="Go to Details"
        onPress={() => navigation.navigate('Home2')}
      />
      <Text style={{color: 'white'}}>MyComponen2t</Text>
    </SafeAreaView>
  );
};

export const Textfeild = props => {
  return (
    <Text
      style={{
        color: props.color || 'white',
        fontSize: props.fontSize || 20,
        fontWeight: props.fontWeight || '200',
      }}>
      {props.content}
    </Text>
  );
};

export const InputBox = props => {
  return (
    <TextInput
      key={'2'}
      maxLength={props.maxLength || 10}
      keyboardType="numeric"
      style={{
        fontSize: props.fontSize,

        letterSpacing: props.letterSpacing || 2,

        alignItems: props.alignItems || 'center',
        justifyContent: props.justifyContent || 'center',
        textAlign: props.textAlign || 'center',

        width: props.width || 300,
        borderColor: props.borderColor || '#E8E8E8',
        color: props.color || '#E8E8E8',
        borderWidth: props.borderWidth || 1,
        borderRadius: props.borderRadius || 10,
        ...padding(
          props.paddingTop,
          props.paddingBottom,
          props.paddingLeft,
          props.paddingRight,
        ),
        ...margin(
          props.marginTop,
          props.marginBottom,
          props.marginLeft,
          props.marginRight,
        ),
      }}
      onChangeText={props.onChangeText}
      value={props.value || ''}
    />
  );
};

export const ButtonFeild = (props) => {
  return (
    <TouchableOpacity onPress={props.onPress}>
      <View
        style={{
          backgroundColor: props.background || 'white',
          borderRadius: props.borderRadius || 5,
          color: props.color || 'black',
          width: props.width || 300,
          alignItems: props.alignItems || 'center',
          justifyContent: props.justifyContent || 'center',
          textAlign: props.textAlign || 'center',

          fontSize: props.fontSize,

          letterSpacing: props.letterSpacing || 2,

          ...padding(
            props.paddingTop,
            props.paddingBottom,
            props.paddingLeft,
            props.paddingRight,
          ),
          ...margin(
            props.marginTop,
            props.marginBottom,
            props.marginLeft,
            props.marginRight,
          ),
        }}>
        <Text
          style={{
            fontSize: props.fontSize || 16,
            color: props.color || 'black',
            letterSpacing: props.letterSpacing || 2,
            fontWeight: props.fontWeight ||'400',
            fontSize: props.fontSize||16
          }}>
          {props.content || 'submit'}
        </Text>
      </View>
    </TouchableOpacity>
  );
};

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
    backgroundColor: '#23232F',
  },

  containerCenter: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#23232F',
  },
  white: {
    color: 'white',
  },
  inputBox: {
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',

    width: 300,
    borderColor: '#E8E8E8',
    color: '#E8E8E8',
    borderWidth: 1,
    borderRadius: 10,
  },
});
