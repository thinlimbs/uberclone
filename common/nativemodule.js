//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet ,Button,Alert} from 'react-native';

// create a component
const NativeModule = ({navigation}) => {
    const handleNativePart=()=>{
        Alert.alert('77')
    }
    return (
        <View style={styles.container}>
            <Text>NATIVE</Text>
            <Button
        title="Go To React Native"
        onPress={() => navigation.navigate('Home')}
      />
         <Button
        title="Handle Native"
        onPress={() =>handleNativePart()}
      />
        </View>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#2c3e50',
    },
});

//make this component available to the app
export default NativeModule;
