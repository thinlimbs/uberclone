import React, {useState} from 'react';
import {
  View,
  Text,
  Button,
  TextInput,
  StyleSheet,
  SafeAreaView,
  Image,
  TouchableOpacity,
} from 'react-native';
import {styles} from '../../common'
import Otp from '../../common/login/getotp'
function HomeScreen({navigation}) {
    const [loginStep, setLoginStep] = useState('otp');
  
    const handleOtp = () => {
      // alert(8)
      setLoginStep('fillOtp');
    };
  
    const checkNum = '1234567890';
    // alert(checkNum.indexOf('3'))
    const [number, setNumber] = useState();
    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.container}>
          <Image
            style={{
              height: 300,
  
              width: 400,
            }}
            source={require('../../img/tax.png')}
          />
        </View>
  
        {loginStep == 'otp' && (
          <View style={styles.containerCenter}>
            <TextInput
              key={'2'}
              maxLength={10}
              onChangeText={text =>
                checkNum.indexOf(text) > -1 ? setNumber(text) : setNumber(text)
              }
              keyboardType="numeric"
              value={number}
              style={{
                height: 40,
                alignItems: 'center',
                justifyContent: 'center',
                textAlign: 'center',
  
                width: 300,
                borderColor: '#E8E8E8',
                color: '#E8E8E8',
                borderWidth: 1,
                borderRadius: 10,
              }}
            />
            <TouchableOpacity
              onPress={() => handleOtp()}
              style={{
                shadowColor: '#000',
                shadowOffset: {
                  width: 0,
                  height: 3,
                },
                shadowOpacity: 0.27,
                shadowRadius: 4.65,
  
                elevation: 6,
              }}>
              <View
                style={{
                  shadowColor: '#000',
                  shadowOffset: {
                    width: 0,
                    height: 2,
                  },
                  shadowOpacity: 0.25,
                  shadowRadius: 4.84,
  
                  elevation: 10,
                  height: 40,
                  marginTop: 10,
                  alignItems: 'center',
                  justifyContent: 'center',
                  borderRadius: 12,
                  color: '#E8E8E8',
                  width: 200,
                  backgroundColor: '#E8E8E8',
                }}>
                <Text
                  style={{
                    alignItems: 'center',
                    justifyContent: 'center',
  
                    color: 'black',
  
                    backgroundColor: '#E8E8E8',
                  }}>
                  Get Otp{' '}
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        )}
  
        {loginStep == 'fillOtp' && <Otp navigation={navigation} />}
      </SafeAreaView>
    );
  }

  export default HomeScreen;