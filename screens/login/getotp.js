//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';

// create a component
const Getotp = () => {
    return (
        <View style={styles.container}>
            <Text style={...styles.white,styles.container}>Getotp</Text>
        </View>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#262632',
    },
});

//make this component available to the app
export default Getotp;
