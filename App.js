// In App.js in a new project

import React, {useState} from 'react';
import {
  View,
  Text,
  Button,
  TextInput,
  StyleSheet,
  SafeAreaView,
  Image,
  TouchableOpacity,
} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import HomeBooking from './screens/homebooking/homebooking'
import {MyComponent, MyComponent2, styles} from './common/index';
import HomeScreen from './screens/home/homescreen'
// import { useState } from 'react';
import Otp from './common/login/getotp';

function HomeScreen2({navigation}) {
  const [loginStep, setLoginStep] = useState('otp');

  const handleOtp = () => {
    // alert(8)
    setLoginStep('fillOtp');
  };

  const checkNum = '1234567890';
  // alert(checkNum.indexOf('3'))
  const [number, setNumber] = useState();
  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.container}>
        <Image
          style={{
            height: 300,

            width: 400,
          }}
          source={require('./img/tax.png')}
        />
      </View>

      {loginStep == 'otp' && (
        <View style={styles.containerCenter}>
          <TextInput
            key={'2'}
            maxLength={10}
            onChangeText={text =>
              checkNum.indexOf(text) > -1 ? setNumber(text) : setNumber(text)
            }
            keyboardType="numeric"
            value={number}
            style={{
              height: 40,
              alignItems: 'center',
              justifyContent: 'center',
              textAlign: 'center',

              width: 300,
              borderColor: '#E8E8E8',
              color: '#E8E8E8',
              borderWidth: 1,
              borderRadius: 10,
            }}
          />
          <TouchableOpacity
            onPress={() => handleOtp()}
            style={{
              shadowColor: '#000',
              shadowOffset: {
                width: 0,
                height: 3,
              },
              shadowOpacity: 0.27,
              shadowRadius: 4.65,

              elevation: 6,
            }}>
            <View
              style={{
                shadowColor: '#000',
                shadowOffset: {
                  width: 0,
                  height: 2,
                },
                shadowOpacity: 0.25,
                shadowRadius: 4.84,

                elevation: 10,
                height: 40,
                marginTop: 10,
                alignItems: 'center',
                justifyContent: 'center',
                borderRadius: 12,
                color: '#E8E8E8',
                width: 200,
                backgroundColor: '#E8E8E8',
              }}>
              <Text
                style={{
                  alignItems: 'center',
                  justifyContent: 'center',

                  color: 'black',

                  backgroundColor: '#E8E8E8',
                }}>
                Get Otp{' '}
              </Text>
            </View>
          </TouchableOpacity>
        </View>
      )}

      {loginStep == 'fillOtp' && <Otp navigation={navigation} />}
    </SafeAreaView>
  );
}

const Neo = ({children, size, style}) => {
  return (
    <View style={styles2.topSadow}>
      <View style={styles2.bottomSadow}>
        <View
          style={[
            styles2.inner,
            {
              width: size || 40,
              height: size || 40,
              borderRadius: size / 2 || 40 / 2,
            },
            style,
          ]}>
          {children}
        </View>
      </View>
    </View>
  );
};

const Stack = createNativeStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{
          headerShown: false,
        }}>
        <Stack.Screen
          options={{title: 'My home'}}
          name="Home"
          component={HomeScreen}
        />
        <Stack.Screen
          options={{headerShown: false}}
          name="Home2"
          component={MyComponent}
        />

<Stack.Screen
          options={{headerShown: false}}
          name="homebooking"
          component={HomeBooking}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;

const styles2 = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#262632',
  },

  container2: {
    flex: 1,
    alignItems: 'center',

    justifyContent: 'flex-end',
    backgroundColor: '#262632',
  },
  topConaiter: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  inner: {
    backgroundColor: '#262632',
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: '#4a4848',
  },

  topSadow: {
    shadowOffset: {
      width: -6,
      height: -6,
    },
    shadowOpacity: 1,
    shadowRadius: 6,
    shadowColor: '#282828',
  },
  bottomSadow: {
    shadowOffset: {
      width: 6,
      height: 6,
    },
    shadowOpacity: 1,
    shadowRadius: 6,
    shadowColor: '#282828',
  },
});
